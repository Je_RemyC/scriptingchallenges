# Define the Switches

param (
    [switch]$Hotfix = $false,
    [switch]$Apps = $false,
    [switch]$export = $false,
    $display = $true
)

# Define the Functions ------------------------------------------------------------------------------------
function Get-HotfixesInstalled () {

    [array] $hotfixesInstalled = @()

    $hotfixesInstalled = Get-Hotfix
  
    return $hotfixesInstalled
  
}

function Get-InstalledApps ([string] $hklmString) {

    [array] $appsInstalled = @()

    $appsInstalled = Get-ItemProperty $hklmstring
  
    return $appsInstalled

}

function CheckOSType () {

    [array] $OSType = @()

    $OSType = [environment]::Is64BitOperatingSystem

    return $OSType

}

# Define variables used when collecting results ---------------------------------------------------------

    [string] $hklmRegKey64 = ""
    [string] $hklmRegKey32 = ""
    [array] $InstalledHotfixes = @()
    [array] $InstalledApps = @()

    $hklmRegKey64 = "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*"
    $hklmRegKey32 = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*"

# Collect the results -----------------------------------------------------------------------------------------------

if ($Hotfix) {
    
    $InstalledHotfixes = Get-HotfixesInstalled
    
}

if ($Apps) {

    If ($OSType = $true) {

        $installedApplications = Get-InstalledApps $hklmRegKey64

    }

        elseif ($OSType = $false) {

            $installedApplications = Get-InstalledApps $hklmRegKey32

        }

}

# Create table headings for custom objects used when collecting results

$installedApplicationsTable = @()

$installedApplicationsColumns = [ordered] @{
    "DisplayName" = ""
    "DisplayVersion" = ""
    "InstallDate" = ""
}

$installedHotfixesTable = @()

$installedHotfixesColumns = [ordered] @{
    "DisplayName" = ""
    "HotFixID" = ""
    "InstallDate" = ""
}

# Collect the results into custom objects ---------------------------------------------------

if ($display) {
    
    if ($Hotfix) {
        
        foreach ($patch in $installedHotfixes) {

            $installedHotfixesColumns."DisplayName" = $patch.Description
            $installedHotfixesColumns."HotFixID" = $patch.HotFixID
            $installedHotfixesColumns."InstallDate" = $patch.InstalledOn
            $objRecord2 = New-Object PSObject -property $installedHotfixesColumns

            $installedHotfixesTable += $objRecord2
            
        }

    }


        if ($Apps) {
            
            foreach ($app in $installedApplications) {
                
                $installedApplicationsColumns."DisplayName" = $app.DisplayName
                $installedApplicationsColumns."DisplayVersion" = $app.DisplayVersion
                $installedApplicationsColumns."InstallDate" = $app.InstallDate
                $objRecord = New-Object PSObject -property $installedApplicationsColumns

                $installedApplicationsTable += $objrecord
  
            }
       
    }

}

# If Exporting results, don't display any results on the console ----------------------------------------------------------

$resultsExportPath = "C:\Users\Public\Documents"
$date = Get-Date -f 'ddMM'

if ($export) {

        $display = $false

        if ($Hotfix) {
            
            $installedHotfixesTable | export-csv -path $resultsExportPath\Hotfixes$date.csv

            }

            if ($Apps) {

                $installedApplicationsTable | export-csv -path $resultsExportPath\Apps$date.csv
      
                }

}

# If results are not being exported, display them on the console
 
  if ($display) {

        $export = $false

        if ($Hotfix) {
            
            $installedHotfixesDisplayCounter = 1
            $installedHotfixesTable
            
            }

            if ($Apps) {

                $installedApplicationsDisplayCounter = 1
                $installedApplicationsTable
                    
                }

# Prevent AppsInstalled Results and HotfixesInstalled results being displayed on the console at the same time
# The custom objects don't play well together when called to display at the same time, so until I resolve this, 
# the following is preventative

            $preventDisplayingTwoResults = $installedApplicationsDisplayCounter + $installedHotfixesDisplayCounter

            if ($preventDisplayingTwoResults -gt 1) {

            Write-Host "Choose one switch at a time when displaying to the console. Both switches can be used at the same time if exporting to CSV" -ForegroundColor Green

            break

          }

}

    



    









        
    

    



    









